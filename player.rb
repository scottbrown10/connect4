class Player

  @@player_list = []
  @@num_of_players = 0
  @@current_player = 0

  attr_accessor :playername, :wins
  attr_accessor :wins
  attr_reader :id, :symbol

  def initialize
    @@num_of_players += 1
    @id = @@num_of_players
    @playername = "Player #{@@num_of_players}"
    @wins = 0
    @@player_list << self # add to array of players
    @symbol = @@num_of_players == 1 ? 'A' : 'B' # board piece
  end

  def self.stats # print players' wins
    @@player_list.each do |p|
      opt_s = p.wins == 1 ? '' : 's'
      puts "#{p.playername}: #{p.wins} win#{opt_s}"
    end
  end

  def self.current # get current player
    @@player_list[@@current_player]
  end

  def self.change # alternate players
    @@current_player = @@current_player == 0 ? 1 : 0
  end

  def self.show_players # show player names
    puts "\nPlayers: "
    @@player_list.each do |p|
      puts p.playername
    end
  end

  def self.change_name
    num = 0
    while num != 1 && num != 2
      puts "\nEnter player number(1 or 2):\n"
      num = gets.to_i
    end

    Player.change if Player.current.id != num
    name = nil
    until name
      puts 'Enter player name:'
      name = gets.chomp

      if name.empty?
        puts 'No blank names.'
        name = nil
        next
      end

      Player.change
      next unless Player.current.playername == name
      puts 'Names must be different.'
      name = nil
      Player.change
    end

    Player.change
    Player.current.playername = name
  end

  def self.reset
    Player.current.wins = 0
    Player.change
    Player.current.wins = 0
  end
end
