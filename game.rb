require_relative 'board'
require_relative 'player'

class Game
  private_class_method :new # use singleton pattern
  @@game = nil
  @@num_games = 0

  def self.create(opt = 0)
    return @@game if @@game
    @@game = opt.zero? ? new : new(1)
  end

  def initialize(opt = 0)
    @b = opt.zero? ? Board.new : Board_2.new
    @ties = 0
  end

  def stats
    Player.stats
    puts "Games played: #{@@num_games}"
    puts "Ties: #{@ties}\n"
  end

  def find_winner
    49.times do
      return true if move # when someone wins, finish
    end
    false
  end

  def play # start a game
    @b.display

    unless find_winner
      puts "It's a tie!"
      @ties += 1
    end

    @@num_games += 1
    stats
    @b.clear
  end

  def move # give a player a move
    @b.add_piece(Player.current)
    @b.display

    if @b.check(Player.current)
      Player.current.wins += 1
      return true
    end

    Player.change
    false
  end

  def reset # reset game, tie, and win counts
    @b.games = 0
    @b.ties = 0
    Player.reset
    puts "\nAll counters reset."
  end

  def instructions
    File.open('instructions.txt', 'r') do |f|
      f.readlines.each do |line|
        puts line
      end
    end
  end
end
