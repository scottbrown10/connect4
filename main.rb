require_relative 'game'

2.times { Player.new }

if ARGV.shift
  puts 'Special Mode'
  G = Game.create(1)
else
  G = Game.create
end

OPTIONS = [
  'Instructions',
  'Display Names',
  'Change Names',
  'Play Game',
  'Display Scores',
  'Reset Scores',
  'Quit'
].freeze

def menu
  num = 0
  puts "\nConnect 4 Main Menu\n"

  while num < 1 || num > 7
    OPTIONS.each_with_index { |opt, i| puts "#{i + 1} - #{opt}" }
    puts "\nEnter option number: "
    num = gets.to_i
  end

  num
end

loop do
  begin
    opt = menu

    case opt
    when 1
      G.instructions
    when 2
      Player.show_players
    when 3
      Player.change_name
    when 4
      G.play
    when 5
      G.stats
    when 6
      G.reset
    when 7
      puts 'Goodbye'
      exit
    end

  rescue Interrupt
    break
  end
end
