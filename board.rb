class Board
  DIM = 7
  attr_accessor :games, :ties, :num_pieces

  def initialize
    @board = Array.new(DIM) { Array.new(DIM) } # board is square
    @ties = 0
    @num_pieces = 0 # num of pieces played
  end

  def clear # reset board
    DIM.times do |i|
      DIM.times do |j|
        @board[i][j] = nil
      end
    end
    @num_pieces = 0
  end

  def display # print board
    DIM.times { |row| display_row(row) }

    # print row numbers at bottom
    DIM.times { |i| print "#{i + 1} " }
    puts
  end

  def display_row(row_num)
    row = @board[row_num]
    DIM.times do |col|
      piece = row[col] || '_'
      print "#{piece} "
    end
    puts
  end

  def column_pick
    col = -1
    while col < 0 || col > DIM
      puts 'Enter column number: '
      col = gets.to_i - 1
      unless @board[0][col].nil?
        puts 'Column Full'
        col = -1
      end
    end
    col
  end

  def add_piece(player) # let player place a piece
    col = column_pick
    # determine where to place piece
    i = DIM - 1
    while i > -1 # start checking from bottom row
      if (@board[i][col]).nil?
        # place user's pieces in bottm row of column
        @board[i][col] = player.symbol
        @num_pieces += 1
        return
      end
      i -= 1
    end
  end

  def check(player) # check if the current user has won
    if check_(player) || check_d(player)
      puts "#{player.playername} wins!\n"
      return true
    end
  end

  def check_(player) # horizontal and vertical check
    DIM.times do |i|
      DIM.times do |j|
        next unless @board[i][j] == player.symbol
        # check for user's piece in 4 adjacent columns
        k = 1
        while k < 4
          break if j + k == DIM
          break if @board[i][j + k] != player.symbol
          k += 1
        end

        return true if k == 4

        # check for user's piece in 4 adjacent rows
        k = 1
        while k < 4
          break if i + k == DIM
          break if @board[i + k][j] != player.symbol
          k += 1
        end
        return true if k == 4
      end
    end
    false
  end

  def check_d(player) # diagonal checks
    return true if check_up_left(player)
    return true if check_up_right(player)
    # checking down left and right is unnecessary. they are covered by checking up left and right
  end

  def check_up_left(player)
    DIM.times do |i|
      DIM.times do |j|
        # starting point needs to be current players piece
        next unless @board[i][j] == player.symbol
        k = 1
        while k < 4 # from current space to 3 spaces away diagonally
          break if @board[i - k][j - k] != player.symbol # move up and left
          k += 1
        end
        # means all checked spots have pieces of current player
        return true if k == 4
      end
    end
    false
  end

  def check_up_right(player)
    for i in 0...DIM
      for j in 0...DIM
        if @board[i][j] != player.symbol
          next
        end
        k = 1
        while k<4 do
          if @board[i-k][j+k] != player.symbol
            break
          end
          k+=1
        end
        if k==4
          return true
        end
      end
    end
    return false
  end

end

class Board_2 < Board # special board. pieces go in from bottom up

  def add_piece(player)
      col = 0
      while col < 1 || col > DIM
        puts "Enter column number: "
        col = gets.to_i
        if @board[DIM-1][col-1] != nil
          puts "Column Full"
          col = 0
        end
      end

      # insert from bottom
      i = 1
      while i < DIM+1  # start checking from top row, move down
        if (@board[i-1][col-1]).nil?
          @board[i-1][col-1] = player.symbol # place user's pieces in row furthest in
          @num_pieces+=1
          return
        end
        i+=1
      end
  end
end
