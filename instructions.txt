Instructions

Players alternate turns dropping their pieces in the board.
Enter the column number of where you wish to place your piece.
The piece will drop to the lowest row possible in that column.
Whoever connects 4 of their pieces in a row (horizontally, vertically, or diagonally) wins.

Enter a command line argument to play in special mode.
In this mode, pieces are inserted from the bottom and go as far up as possible.
